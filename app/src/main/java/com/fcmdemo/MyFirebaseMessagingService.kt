package com.fcmdemo

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyFirebaseMessagingService:FirebaseMessagingService() {
    private var notifyPendingIntent: PendingIntent? = null
    private var notificationManager: NotificationManager? = null
    private lateinit var broadcaster: LocalBroadcastManager
    val ANDROID_CHANNEL_NAME = "ANDROID CHANNEL"
    val IOS_CHANNEL_NAME = "IOS CHANNEL"
    val ANDROID_CHANNEL_ID = "com.fcmdemo.ANDROID"
    val IOS_CHANNEL_ID = "com.fcmdemo.IOS"
    var groupId = "group_id_101"
    var groupName: CharSequence = "FCM DEMO GROUP"


    override fun onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this)
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)

//        val i = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            Intent(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS).apply {
//                putExtra(Settings.EXTRA_APP_PACKAGE, packageName)
//                putExtra(Settings.EXTRA_CHANNEL_ID, ANDROID_CHANNEL_ID)
//            }
//        } else {
//            Intent(this, MainActivity::class.java)
//        }

        val i = Intent(this, MainActivity::class.java)
        notifyPendingIntent = PendingIntent.getActivity(this, 1001, i, PendingIntent.FLAG_UPDATE_CURRENT)

//        p0.notification?.let {
//            Log.d("MyFCMService", "Message Notification Body: ${it.body}")
//        }
//
//        p0.data.isNotEmpty().let {
//            Log.d("MyFCMService", "Message data payload: " + p0.data)
//        }

        val dataKey = p0.data

        if (isAppIsInBackground(this))
            showNotification("FCM Demo Test Title", dataKey["message"] ?: "")
        else
        {
            val intent = Intent("FCM_DEMO_DATA")
            intent.putExtra("demoData", dataKey["message"] ?: "")
            broadcaster.sendBroadcast(intent)
        }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
    }

    private fun showNotification(title: String, body: String) {
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            notificationManager?.createNotificationChannelGroup(NotificationChannelGroup(groupId, groupName))

            //            val audioAttributes = AudioAttributes.Builder()
//                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
//                .build()

            val mChannel = NotificationChannel(ANDROID_CHANNEL_ID, ANDROID_CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH)
            mChannel.enableVibration(true)
//            mChannel.setSound(Uri.parse(("android.resource://"+this.packageName)+"/" + R.raw.noti_sound),audioAttributes)
            mChannel.setShowBadge(true)
            mChannel.vibrationPattern = longArrayOf(4000)
            mChannel.enableLights(true)
            mChannel.lightColor = Notification.COLOR_DEFAULT
            mChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            mChannel.setShowBadge(true)
//            mChannel.group = groupId
            notificationManager!!.createNotificationChannel(mChannel)

            val nb = getAndroidChannelNotification(title, body)
            nb?.let {
                notificationManager?.notify((1..20).random(), it.build())
            }

//            val mChannelIOS = NotificationChannel(IOS_CHANNEL_ID, IOS_CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH)
//            mChannelIOS.enableVibration(true)
////            mChannel.setSound(Uri.parse(("android.resource://"+this.packageName)+"/" + R.raw.noti_sound),audioAttributes)
//            mChannelIOS.setShowBadge(true)
//            mChannelIOS.vibrationPattern = longArrayOf(4000)
//            mChannelIOS.enableLights(true)
//            mChannelIOS.lightColor = Notification.COLOR_DEFAULT
//            mChannelIOS.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
//            mChannelIOS.setShowBadge(true)
//            mChannelIOS.group = groupId
//            notificationManager!!.createNotificationChannel(mChannelIOS)
//
//            val nbIOS = getIOSChannelNotification(title, body)
//            nbIOS?.let {
//                notificationManager?.notify((1..20).random(), it.build())
//            }

        } else {
            val nb = getAndroidChannelNotification(title, body)
            nb?.let {
                notificationManager?.notify((1..20).random(), it.build())
            }
//            val nbIOS = getIOSChannelNotification(title, body)
//            nbIOS?.let {
//                notificationManager?.notify((1..20).random(), it.build())
//            }
        }

    }

    private fun getAndroidChannelNotification(title: String?, body: String?): NotificationCompat.Builder? {
        return NotificationCompat.Builder(applicationContext, ANDROID_CHANNEL_ID)
                .setContentTitle(title)
                .setStyle(NotificationCompat.BigTextStyle().bigText(body))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLights(Color.WHITE, 3000, 1000)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(notifyPendingIntent)

//                .setSound(Uri.parse(("android.resource://"+this.packageName)+"/" + R.raw.noti_sound))
    }


    private fun getIOSChannelNotification(title: String?, body: String?): NotificationCompat.Builder? {
        return NotificationCompat.Builder(applicationContext, IOS_CHANNEL_ID)
                .setContentTitle(title)
                .setStyle(NotificationCompat.BigTextStyle().bigText(body))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLights(Color.WHITE, 3000, 1000)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(notifyPendingIntent)

//                .setSound(Uri.parse(("android.resource://"+this.packageName)+"/" + R.raw.noti_sound))
    }


    private fun isAppIsInBackground(context: Context): Boolean {
        var isInBackground = true
        try {
            val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val runningProcesses = am.runningAppProcesses
            for (processInfo in runningProcesses)
            {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND)
                {
                    for (activeProcess in processInfo.pkgList) {

                        if (activeProcess == context.packageName)
                            isInBackground = false
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return isInBackground
    }

}