package com.fcmdemo

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("MyFCMService", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            val token = task.result
            Log.d("MyFCMService", token?:"")
        })
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(bookingRequestReceiver, IntentFilter("FCM_DEMO_DATA"))
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(bookingRequestReceiver)
        super.onPause()
    }

    private val bookingRequestReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            try {
                if (intent != null) {
                   intent.getStringExtra("demoData")?.let {
                       Toast.makeText(this@MainActivity,it,Toast.LENGTH_SHORT).show()
                   }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}